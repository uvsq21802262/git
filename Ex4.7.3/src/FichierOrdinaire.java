
public class FichierOrdinaire extends Fichier{

	private int size;
	
	/**
	 * @param name : Constructeur avec en param�tre le nom du fichier.
	 * La taille d'un fichier est de 1.
	 */
	public FichierOrdinaire(String name) {
		super(name);
		this.size = 1;
		// TODO Auto-generated constructor stub
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	
}
