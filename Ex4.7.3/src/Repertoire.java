import java.util.ArrayList;

public class Repertoire extends Fichier{
	
	/**
	 * ArrayList<Fichier> L : l'ensemble des r�pertoires et fichiers d'un r�pertoire.
	 * private int size; : La taille du r�pertoire.
	 */
	ArrayList<Fichier> L = new ArrayList<Fichier>();
	private int size;
	
	/**
	 * @param name : Le constrcteur re�oit le nom du r�pertoire puis
	 * met � 0 la taille du dossier.
	 */
	public Repertoire(String name) {
		super(name);
		this.size = 0;
		
	}

	/**
	 * @param F : Re�oit le fichier F puis l'ajoute au r�pertoire et met � jour
	 * la taille du r�pertoire.
	 */
	public void addFichierOrdinaire(FichierOrdinaire F)
	{
		L.add(F);
		//super.setSize(super.getSize() + 1);
		this.size = this.getSize() + F.getSize();
		System.out.println("Fichier Ajout�");	
	}
	
	/**
	 * @param R : Re�oit le r�pertoire puis l'ajoute dans le r�pertoire puis met � jour 
	 * la taille du r�peroire.
	 */
	public void ajouterRepertoire(Repertoire R)
	{
		L.add(R);
		this.size = this.getSize() + R.getSize();
		System.out.println("R�pertoire Ajout�");
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	
	
	/**
	 * Cette fonction calcule la taille d'un r�pertoire.
	 * @param R : Un r�pertoire
	 * @return : La taille totale du r�pertoire R
	 */
	public int calculTaille(Repertoire R)
	{
		int nb = 0;
		for(Fichier F : R.L)
		{
			if (F.getClass().equals(FichierOrdinaire.class))
			{
				FichierOrdinaire f = (FichierOrdinaire) F;
				nb += f.getSize();
			}else
			{
				Repertoire r = (Repertoire) F;
				nb += r.getSize();
			}
		}
		return nb;
	}
	
}