
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		//Calcul de la taille du repertoire:
		
		//Ajout simple d'un fichier dans un répertoire 
		Repertoire R = new Repertoire("Racine");
		FichierOrdinaire F = new FichierOrdinaire("secouba");
		R.addFichierOrdinaire(F);
		
		//Ajout d'un répertoire dans un répertoire contenant un fichier
		Repertoire R1 = new Repertoire("secouba2");
		Repertoire R2 = new Repertoire("secouba3");
		FichierOrdinaire F1 = new FichierOrdinaire("sara");
		R2.addFichierOrdinaire(F1);
		R1.ajouterRepertoire(R2);
		
		//Ajout d'un répertoire dans un répertoire contenant deux fichiers
		Repertoire R3 = new Repertoire("secouba2");
		Repertoire R4 = new Repertoire("secouba3");
		FichierOrdinaire F2 = new FichierOrdinaire("sara");
		FichierOrdinaire F3 = new FichierOrdinaire("sara2");
		R4.addFichierOrdinaire(F2);
		R4.addFichierOrdinaire(F3);
		R3.ajouterRepertoire(R4);
		
		
		
		System.out.println(R3.getSize());
		
		
		
	}

}
