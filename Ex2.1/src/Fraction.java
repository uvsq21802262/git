public class Fraction {


    private float a;
    private float b;

    public Fraction(float a, float b) {
        this.a = a;
        this.b = b;
    }



    public float getB() {
        return b;
    }

    public void setB(float b) {
        this.b = b;
    }



    public float getA() {
        return a;
    }

    public void setA(float a) {
        this.a = a;
    }

    float diff()
    {
        if (a != 0)return a/b;
        else
            return 0;
    }


}
